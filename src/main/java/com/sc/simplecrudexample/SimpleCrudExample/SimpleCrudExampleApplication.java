package com.sc.simplecrudexample.SimpleCrudExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

//Used to tell springboot that these are the paths where you can create beans from
@EntityScan("com.sc.simplecrudexample.entity")
@EnableJpaRepositories("com.sc.simplecrudexample.repository")
@ComponentScan("com.sc.simplecrudexample")
@SpringBootApplication
public class SimpleCrudExampleApplication {

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(SimpleCrudExampleApplication.class, args);
	}

}
