package com.sc.simplecrudexample.services;

import java.util.ArrayList;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sc.simplecrudexample.dto.ResponseDto;
import com.sc.simplecrudexample.dto.SongDto;
import com.sc.simplecrudexample.entity.SongEntity;
import com.sc.simplecrudexample.exceptions.NoSuchSongException;
import com.sc.simplecrudexample.exceptions.SomethingWentWrong;
import com.sc.simplecrudexample.repository.SimpleRepository;


@Service
public class SimpleServiceImpl implements SimpleServiceInterface {

	@Autowired
	SimpleRepository simpleRepo;

	@Autowired
	RestTemplate restTemplate;

	Logger log = Logger.getLogger(SimpleServiceImpl.class);
	
	
	
	/**
	 * Get list of all songs
	 * 
	 * @author pooja.kommi
	 * @return ResponseDto
	 */
	@Override
	public ResponseDto getListOfSongs() {

		ResponseDto responseDto = new ResponseDto();

		try {
			List<SongEntity> listOfSongsEntity = simpleRepo.findAll();

			List<SongDto> listOfSongsDTO = new ArrayList<>();

			for (SongEntity se : listOfSongsEntity) {
				SongDto songdto = new SongDto();
				BeanUtils.copyProperties(se, songdto);
				listOfSongsDTO.add(songdto);
			}
			// responseDto.setSongDtoList(listOfSongsDTO);
			responseDto.setStatusCode(HttpStatus.OK.value());
			responseDto.setMessage("Successfully retrieved");
			log.info("List of songs obtained");
		} catch (Exception e) {
			responseDto.setStatusCode(HttpStatus.EXPECTATION_FAILED.value());
			responseDto.setMessage("Failed! Could not retrieve song list!!!");
			log.info("Could not retrieve song list");
		}
		return responseDto;
	}

	
	
	/**
	 * Add song
	 * @author pooja.kommi
	 * @return ResponseDto
	 */
	@Override
	public ResponseDto addSong(SongDto songDto) {
		SongEntity songEntity = new SongEntity();
		ResponseDto responseDto = new ResponseDto();

		if (songDto == null) {
			responseDto.setMessage("Fill in some values yooo!");
			responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());

		} else {
			BeanUtils.copyProperties(songDto, songEntity);
			//simpleRepo.save(songEntity);
			SongEntity isSaved = simpleRepo.save(songEntity);
			if (isSaved != null) {
				responseDto.setMessage("Yay! Successfully added!!!");
				responseDto.setStatusCode(HttpStatus.CREATED.value());
				log.info("Successfully added!");
			} else {
				responseDto.setMessage("Nope,something went wrong,Sorry!");
				responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
				log.error("Something went wrong, could not add a song.");
			}
		}
		return responseDto;
	}

	
	/**
	 * @author pooja.kommi
	 * update a song
	 * @param songDto
	 * @param songId
	 * @return ResponseDto
	 * @throws SomethingWentWrong
	 */
	@Override
	public ResponseDto updateSong(SongDto songDto, int songId) throws SomethingWentWrong {
		SongEntity songEntity = new SongEntity();
		ResponseDto responseDto = new ResponseDto();
		if(songDto==null)
		{
			log.error("User didn't enter anything, failed to update");
			throw new SomethingWentWrong("You haven't filled in anything!"); 
		}
		else
		{
			BeanUtils.copyProperties(songDto, songEntity);
			simpleRepo.save(songEntity);
			
			SongEntity isSaved = simpleRepo.save(songEntity);

			if(isSaved!=null)
			{
				responseDto.setMessage("Yay! Successfully added!!!");
				responseDto.setStatusCode(HttpStatus.CREATED.value());
				log.info("updated successfully!!!");
			}
			else
			{
				log.error("Something went wrong!");
				throw new SomethingWentWrong("Something went wrong!"); 
			}
		}
		return responseDto;
	}

	
	/**
	 * @author pooja.kommi
	 * delete a song, given the id.
	 * @param songId
	 * @return ResponseDto
	 * @throws NoSuchSongException
	 */
	@Override
	public ResponseDto removeSong(int songId) throws NoSuchSongException {
		ResponseDto responseDto = new ResponseDto();
		if(!simpleRepo.findById(songId).isPresent())
		{
			log.error("No such song exists, cannot delete!");
			throw new NoSuchSongException("No such song exists yo!"); 

		}
		else
		{
			simpleRepo.deleteById(songId);
			responseDto.setMessage("Deleted!");
			responseDto.setStatusCode(HttpStatus.OK.value());
			log.info("song has been successfully deleted!");
		}
		return responseDto;
	}

}
