package com.sc.simplecrudexample.services;

import java.util.ArrayList;

import java.util.List;

import com.sc.simplecrudexample.dto.RequestDto;
import com.sc.simplecrudexample.dto.ResponseDto;
import com.sc.simplecrudexample.repository.CityRepository;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EligibilityServiceImpl implements EligibilityServiceInterface {

    // @Autowired
    // TierRepository tierRepo;

    @Autowired
    CityRepository cityRepo;

    @Autowired
    RestTemplate restTemplate;

    Logger log = Logger.getLogger(EligibilityServiceImpl.class);

    /**
     * Check eligibility of Mortgage
     * 
     * @author pooja.kommi
     * @return ResponseDto
     */
    @Override
    public ResponseDto checkEligibility(RequestDto requestDto) {
        // SongEntity songEntity = new SongEntity();
        ResponseDto responseDto = new ResponseDto();

        // Check if Montly expense is valid
        if(requestDto.getMonthlyExpense() == null){
            responseDto.setMessage("Invalid Montly Expense");
            responseDto.setStatusCode(422);
            return responseDto;
        }
        if(requestDto.getMonthlyExpense() < 0){
            responseDto.setMessage("Invalid Montly Expense");
            responseDto.setStatusCode(422);
            return responseDto;
        }

        // Check if Montly Income if valid
        if(requestDto.getMonthlyIncome() == null){
            responseDto.setMessage("Invalid Montly Income");
            responseDto.setStatusCode(422);
            return responseDto;
        }
        // Check if Montly Income if valid
        if(requestDto.getMonthlyIncome() < 0){
            responseDto.setMessage("Invalid Montly Income");
            responseDto.setStatusCode(422);
            return responseDto;
        }

        // Check if mortgageDurationInMonths is valid
        if(requestDto.getMortgageDurationInMonths() == null){
            responseDto.setMessage("Invalid Mortgage Duration");
            responseDto.setStatusCode(422);
            return responseDto;
        }
        if(requestDto.getMortgageDurationInMonths() < 0){
            responseDto.setMessage("Invalid Mortgage Duration");
            responseDto.setStatusCode(422);
            return responseDto;
        }

        // Check if expectedMortgageAmt is valid
        if(requestDto.getExpectedMortgageAmt() == null){
            responseDto.setMessage("Invalid Expected Mortgage Amount");
            responseDto.setStatusCode(422);
            return responseDto;
        }
        if(requestDto.getExpectedMortgageAmt() < 0){
            responseDto.setMessage("Invalid Expected Mortgage Amount");
            responseDto.setStatusCode(422);
            return responseDto;
        }

        // Check if pincode is valid
        if(requestDto.getPinCode() == null){
            responseDto.setMessage("Invalid Pincode");
            responseDto.setStatusCode(422);
            return responseDto;
        }
        if(requestDto.getPinCode() < 99999 || requestDto.getPinCode() > 999999){
            responseDto.setMessage("Invalid Pincode");
            responseDto.setStatusCode(422);
            return responseDto;
        }

    
        // Check if dateOfBirth is valid is valid
        if(requestDto.getDateOfBirth() == null){
            responseDto.setMessage("Invalid Date Of Birth");
            responseDto.setStatusCode(422);
            return responseDto;
        }


        Long repaymentStrength = requestDto.getExpectedMortgageAmt() / requestDto.getMortgageDurationInMonths();
        

        responseDto.setMessage("Approved");
        responseDto.setStatusCode(200);

        // if (songDto == null) {
        // responseDto.setMessage("Fill in some values yooo!");
        // responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());

        // } else {
        // BeanUtils.copyProperties(songDto, songEntity);
        // //simpleRepo.save(songEntity);
        // SongEntity isSaved = simpleRepo.save(songEntity);
        // if (isSaved != null) {
        // responseDto.setMessage("Yay! Successfully added!!!");
        // responseDto.setStatusCode(HttpStatus.CREATED.value());
        // log.info("Successfully added!");
        // } else {
        // responseDto.setMessage("Nope,something went wrong,Sorry!");
        // responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
        // log.error("Something went wrong, could not add a song.");
        // }
        // }
        return responseDto;
    }


}
