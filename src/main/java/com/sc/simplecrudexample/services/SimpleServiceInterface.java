package com.sc.simplecrudexample.services;

import org.springframework.stereotype.Service;

import com.sc.simplecrudexample.dto.ResponseDto;
import com.sc.simplecrudexample.dto.SongDto;
import com.sc.simplecrudexample.exceptions.NoSuchSongException;
import com.sc.simplecrudexample.exceptions.SomethingWentWrong;

@Service
public interface SimpleServiceInterface {

	public ResponseDto getListOfSongs();

	public ResponseDto addSong(SongDto song);

	public ResponseDto updateSong(SongDto songDto , int songId) throws SomethingWentWrong;

	public ResponseDto removeSong(int songId) throws NoSuchSongException;
}
