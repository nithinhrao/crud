package com.sc.simplecrudexample.services;

import org.springframework.jmx.access.MBeanConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartException;

import com.sc.simplecrudexample.dto.RequestDto;
import com.sc.simplecrudexample.dto.ResponseDto;
import com.sc.simplecrudexample.dto.SongDto;
import com.sc.simplecrudexample.exceptions.NoSuchSongException;
import com.sc.simplecrudexample.exceptions.SomethingWentWrong;

@Service
public interface EligibilityServiceInterface {

    public ResponseDto checkEligibility(RequestDto request);
}
