package com.sc.simplecrudexample.exceptions;

public class SomethingWentWrong extends Exception {
	
	private static final long serialVersionUID = 1L;
	String message;
	
	public SomethingWentWrong(final String message)
	{
		super(message);
	}

}
