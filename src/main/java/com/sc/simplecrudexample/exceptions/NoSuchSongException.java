package com.sc.simplecrudexample.exceptions;

public class NoSuchSongException extends Exception {
	
	private static final long serialVersionUID = 1L;
	String message;
	
	public NoSuchSongException(final String message)
	{
		super(message);
	}

}
