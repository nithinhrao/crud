package com.sc.simplecrudexample.dto;

import java.util.Set;

public class TierDto {
	private Set<CityDto> cities;
	private Long tierId;
	private Double percentage;

	public Long getTierId() {
		return tierId;
	}

	public void setTierId(Long tierId) {
		this.tierId = tierId;
	}

	public Set<CityDto> getCities() {
		return cities;
    }
    
    public void setCities(Set<CityDto> cities) {
		this.cities = cities;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}
}
