package com.sc.simplecrudexample.dto;

public class CityDto {
	private String cityName;
	private TierDto tier;
	private Long cityId;
	private Long subUrbPinCode;

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public TierDto getTier() {
		return tier;
	}

	public void setTier(TierDto tier) {
		this.tier = tier;
	}

	public Long getSuburbPinCode() {
		return subUrbPinCode;
	}

	public void setSubUrbPinCode(long subUrbPinCode) {
		this.subUrbPinCode = subUrbPinCode;
	}

}
