package com.sc.simplecrudexample.dto;

import java.util.Date;

public class RequestDto {
    private Date dateOfBirth;
    private Long monthlyIncome;
    private Long monthlyExpense;
    private Long pinCode;
    private Long mortgageDurationInMonths;
    private Long expectedMortgageAmt;

    public Long getExpectedMortgageAmt() {
        return expectedMortgageAmt;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Long getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(Long monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public Long getMonthlyExpense() {
        return monthlyExpense;
    }

    public void setMonthlyExpense(Long monthlyExpense) {
        this.monthlyExpense = monthlyExpense;
    }

    public Long getPinCode() {
        return pinCode;
    }

    public void setPinCode(Long pinCode) {
        this.pinCode = pinCode;
    }

    public Long getMortgageDurationInMonths() {
        return mortgageDurationInMonths;
    }

    public void setMortgageDurationInMonths(Long mortgageDurationInMonths) {
        this.mortgageDurationInMonths = mortgageDurationInMonths;
    }

    public void setExpectedMortgageAmt(Long expectedMortgageAmt) {
        this.expectedMortgageAmt = expectedMortgageAmt;
    }

    
}