package com.sc.simplecrudexample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.sc.simplecrudexample.dto.ResponseDto;
import com.sc.simplecrudexample.dto.SongDto;
import com.sc.simplecrudexample.exceptions.NoSuchSongException;
import com.sc.simplecrudexample.exceptions.SomethingWentWrong;
import com.sc.simplecrudexample.services.SimpleServiceInterface;
 
@RestController
public class SimpleController {

	// Use this for post,get,put,delete
	@Autowired
	RestTemplate restTemplate;

	@Autowired
	SimpleServiceInterface songService;

	/**
	 * Get list of all songs
	 * 
	 * @author pooja.kommi
	 * @return ResponseDto
	 */
	@GetMapping("/songs")
	public ResponseDto getListOfSongs() {
		// System.out.println("Getting list of songs");
		ResponseDto responseDto = songService.getListOfSongs();
		return responseDto;
	}

	/**
	 * Add song
	 * 
	 * @author pooja.kommi
	 * @return ResponseEntity<ResponseDto>
	 */
	@PostMapping("/songs")
	public ResponseEntity<ResponseDto> addSong(@RequestBody SongDto songDto) {
		ResponseDto responseDto = songService.addSong(songDto);
		return new ResponseEntity<>(responseDto, HttpStatus.CREATED);

	}

	/**
	 * @author pooja.kommi update a song
	 * @param songDto
	 * @param songId
	 * @return
	 * @throws SomethingWentWrong
	 */
	@PutMapping("/songs/{songId}")
	public ResponseEntity<ResponseDto> updateSong(SongDto songDto, @PathVariable int songId) throws SomethingWentWrong {
		ResponseDto responseDto = songService.updateSong(songDto, songId);
		return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
	}

	/**
	 * @author pooja.kommi delete a song, given the id.
	 * @param songId
	 * @return
	 * @throws NoSuchSongException
	 */
	@DeleteMapping("/songs/{songId}")
	public ResponseDto deleteSong(@PathVariable int songId) throws NoSuchSongException {
		ResponseDto responseDto = songService.removeSong(songId);
		return responseDto;
	}
	
	
}
