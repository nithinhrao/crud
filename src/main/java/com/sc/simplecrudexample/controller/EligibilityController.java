package com.sc.simplecrudexample.controller;

import com.sc.simplecrudexample.dto.RequestDto;
import com.sc.simplecrudexample.dto.ResponseDto;
import com.sc.simplecrudexample.services.EligibilityServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class EligibilityController {

	// Use this for post,get,put,delete
	@Autowired
	RestTemplate restTemplate;

	@Autowired
	EligibilityServiceInterface eligibilityService;


	/**
	 * Check Eligibility
	 * 
	 * @author pooja.kommi
	 * @return ResponseEntity<ResponseDto>
	 */
	@GetMapping("/mortgage")
	public ResponseEntity<ResponseDto> checkEligibility(@RequestBody RequestDto reqDto) {
        ResponseDto responseDto = new ResponseDto();
        responseDto = eligibilityService.checkEligibility(reqDto);
		return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
	}
	
}
