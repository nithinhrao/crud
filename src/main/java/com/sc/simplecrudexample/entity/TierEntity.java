package com.sc.simplecrudexample.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tiers")
public class TierEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	public Long getTierId() {
		return tierId;
	}

	public void setTierId(Long tierId) {
		this.tierId = tierId;
	}

	// public String getTierName() {
	// 	return tierName;
	// }

	// public void setTierName(String tierName) {
	// 	this.tierName = tierName;
	// }

	// public Set<CityEntity> getCities() {
	// 	return cities;
	// }

	// public void setCities(Set<CityEntity> cities) {
	// 	this.cities = cities;
	// }

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tier_id", unique = true)
    private Long tierId;

	// @Column(name = "tier_name", unique = false)
	// private String tierName;

	@Column(name = "percentage", unique = true)
    private Double percentage;

    // @OneToMany(mappedBy = "cities", fetch = FetchType.LAZY,
    //         cascade = CascadeType.ALL)
    // private Set<CityEntity> cities;
}
