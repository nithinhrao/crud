package com.sc.simplecrudexample.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cities")
public class CityEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public TierEntity getTier() {
		return tier;
	}

	public void setTier(TierEntity tier) {
		this.tier = tier;
	}

	public Long getSuburbPinCode() {
		return subUrbPinCode;
	}

	public void setSubUrbPinCode(long subUrbPinCode) {
		this.subUrbPinCode = subUrbPinCode;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "city_id", unique = true)
    private Long cityId;

	@Column(name = "city_name", unique = false)
	private String cityName;

	@Column(name = "suburb_pincode", unique = true)
    private Long subUrbPinCode;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "tier_id", nullable = false)
    private TierEntity tier;
}
