package com.sc.simplecrudexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sc.simplecrudexample.entity.CityEntity;


@Repository
public interface CityRepository extends JpaRepository<CityEntity, Long>{
	
	

}
