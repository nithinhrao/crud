package com.sc.simplecrudexample.repository;

import com.sc.simplecrudexample.entity.TierEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TierRepository extends JpaRepository<TierEntity, Long>{
	
	

}
