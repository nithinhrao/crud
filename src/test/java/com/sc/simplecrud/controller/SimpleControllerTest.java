package com.sc.simplecrud.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.sc.simplecrudexample.controller.SimpleController;
import com.sc.simplecrudexample.dto.ResponseDto;
import com.sc.simplecrudexample.dto.SongDto;
import com.sc.simplecrudexample.exceptions.NoSuchSongException;
import com.sc.simplecrudexample.exceptions.SomethingWentWrong;
import com.sc.simplecrudexample.services.SimpleServiceInterface;

@RunWith(MockitoJUnitRunner.class)
public class SimpleControllerTest {

	@Mock
	SimpleServiceInterface songService;

	@InjectMocks
	SimpleController simpleController;

	SongDto songDto;
	ResponseDto responseDto;
	int songId;

	@Before
	public void DoBefore() {
		responseDto = new ResponseDto();
		responseDto.setMessage("successful");
		responseDto.setStatusCode(200);

		songDto = new SongDto();
		songDto.setSinger("Bob");
		songDto.setSongDuration(5);
		songDto.setSongId(20);
		songDto.setSongName("Bob's Song");
		
		songId = 20;
		
	}

	@Test
	public void getListOfSongs() {
		Mockito.when(songService.getListOfSongs()).thenReturn(responseDto);
		ResponseDto actualValue = simpleController.getListOfSongs();
		Assert.assertEquals("successful", actualValue.getMessage());
	}

	@Test
	public void addSong() {
		Mockito.when(songService.addSong(Mockito.any())).thenReturn(responseDto);
		ResponseEntity<ResponseDto> actualValue = simpleController.addSong(songDto);
		Assert.assertEquals("successful", actualValue.getBody().getMessage());
	}
	
	@Test
	public void updateSong() throws SomethingWentWrong {
		Mockito.when(songService.updateSong((Mockito.any()), Mockito.anyInt())).thenReturn(responseDto);
		ResponseEntity<ResponseDto> actualValue = simpleController.updateSong(songDto, songId);
		Assert.assertEquals("successful", actualValue.getBody().getMessage());
	}
	
	@Test
	public void deleteSong() throws NoSuchSongException
	{
		Mockito.when(songService.removeSong(Mockito.anyInt())).thenReturn(responseDto);
		ResponseDto actualValue = simpleController.deleteSong(songId);
		Assert.assertEquals(200, actualValue.getStatusCode().intValue());
	
	}

}
