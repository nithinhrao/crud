package com.sc.simplecrud.services;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.sc.simplecrudexample.dto.ResponseDto;
import com.sc.simplecrudexample.dto.SongDto;
import com.sc.simplecrudexample.entity.SongEntity;
import com.sc.simplecrudexample.exceptions.NoSuchSongException;
import com.sc.simplecrudexample.exceptions.SomethingWentWrong;
import com.sc.simplecrudexample.repository.SimpleRepository;
import com.sc.simplecrudexample.services.SimpleServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class SimpleServiceImplTest {

	@Mock
	SimpleRepository simpleRepo;

	@InjectMocks
	SimpleServiceImpl simpleServiceImpl;

	
	SongEntity songEntity;
	List<SongEntity> listOfSongEntity;
	SongDto songDto;
	ResponseDto responseDto;
	int songId;
	List<SongDto> songDtoList;


	@Before
	public void setup() {
		songEntity = new SongEntity();
		listOfSongEntity = new ArrayList<SongEntity>();
		songEntity.setSinger("Bob");
		listOfSongEntity.add(songEntity);
		responseDto = new ResponseDto();
		responseDto.setMessage("Yay! Successfully added!!!");
		// responseDto.setSongDtoList(songDtoList);
		
		songId=1;
		
		songDto = new SongDto();
		songDto.setSongId(1);
		songDto.setSinger("Bob");
		songDto.setSongDuration(5);
		songDto.setSongName("Bob's song");
		songDtoList = new ArrayList<SongDto>();
		songDtoList.add(songDto);
	}

	@Test
	public void getSongs() {
		Mockito.when(simpleRepo.findAll()).thenReturn(listOfSongEntity);
		ResponseDto actualValue = simpleServiceImpl.getListOfSongs();
		// Assert.assertEquals(200, actualValue.getStatusCode().intValue());
		// Assert.assertEquals(1, actualValue.getSongDtoList().size());

	}

	@Test 
	public void addSong() {
	  Mockito.when(simpleRepo.save(Mockito.any())).thenReturn(songEntity);
	  ResponseDto actualValue = simpleServiceImpl.addSong(songDto);
	  Assert.assertEquals(responseDto.getMessage(), actualValue.getMessage());
	  }
	
	@Test
	public void updateSong() throws SomethingWentWrong {
		  Mockito.when(simpleRepo.save(Mockito.any())).thenReturn(songEntity);
		  ResponseDto actualValue = simpleServiceImpl.updateSong(songDto, songId);
		  Assert.assertEquals(responseDto.getMessage(), actualValue.getMessage());
		  }
	
	@Test
	public void deleteSong() throws NoSuchSongException {
		/*
		 * simpleServiceImpl.removeSong(songId); verify(simpleRepo,
		 * times(1)).deleteById(songId);
		 */
		}

}
